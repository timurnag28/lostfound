import React, {Component} from 'react';
import RootNavigation from './src/navigation/RootNavigation';
import {SafeAreaView, StatusBar} from 'react-native';
import {NavigationProps} from './src/share/inrefaces';

export default class App extends Component<NavigationProps> {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <StatusBar
          hidden={true}
        />
        <RootNavigation />
      </SafeAreaView>
    );
  }
}
