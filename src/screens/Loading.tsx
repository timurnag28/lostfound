import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet} from 'react-native';
import firebase from 'react-native-firebase';
import {NavigationProps} from '../share/inrefaces';
import {MontserratBold} from '../share/fonts';

export default class Loading extends React.PureComponent<NavigationProps> {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      this.props.navigation.navigate(user ? 'Main' : 'Login');
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color={'#3f5df5'} />
        <Text style={styles.loadingText}>Загрузка...</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingText: {
    fontFamily: MontserratBold,
    fontSize: 15,
    color: '#252D76',
    lineHeight: 22,
  },
});
