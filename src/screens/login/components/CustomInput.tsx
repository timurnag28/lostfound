import React, {useEffect, useRef, useState} from 'react';
import {
  StyleProp,
  StyleSheet,
  Text,
  TextInput,
  View,
  ViewStyle,
} from 'react-native';
import {QuicksandBold, UbuntuBold} from '../../../share/fonts';
import Icon from 'react-native-vector-icons/Feather';
import {WINDOW_WIDTH} from '../../../share/consts';
const borderColor = 'rgba(37, 45, 118, 0.2)';

export const CustomInput = ({
  value,
  title,
  style,
  secureTextEntry,
  onChangeText,
  errorMessage,
}: {
  value: string;
  title?: string;
  style?: StyleProp<ViewStyle>;
  secureTextEntry?: boolean;
  onChangeText: (item: string) => void;
  errorMessage: string;
  disableInputs: boolean;
}) => {
  const [border, setBorderColor] = useState(borderColor);
  const [hidePassword, setHidePassword] = useState(false);

  return (
    <View style={[styles.container, style]}>
      <Text style={styles.title}>{title}</Text>
      <View style={[{borderBottomColor: errorMessage === '' ? border : 'red'}, styles.textInputContainer]}>
        <TextInput
          value={value}
          onChangeText={item => onChangeText(item)}
          secureTextEntry={!hidePassword && secureTextEntry}
          onBlur={() => setBorderColor(borderColor)}
          onFocus={() => setBorderColor('#3066E0')}
          style={styles.textInput}
        />
        {secureTextEntry ? (
          <Icon
            onPress={() => setHidePassword(!hidePassword)}
            name={'eye'}
            size={20}
            color={'#3066E0'}
            style={{paddingBottom: 4}}
          />
        ) : null}
      </View>
      {errorMessage ? <Text style={{color: 'red'}}>{errorMessage}</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: WINDOW_WIDTH,
    paddingHorizontal: 32,
    alignSelf: 'flex-end',
  },
  title: {
    fontSize: 13,
    color: 'rgba(37, 45, 118, 0.5)',
    fontFamily: UbuntuBold,
  },
  textInputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    borderBottomWidth: 1,
  },
  textInput: {
    color: '#252D76',
    fontSize: 14,
    paddingLeft: 0,
    paddingBottom: 0,
    fontFamily: QuicksandBold,
    flex: 1,
  },
});
