import React from 'react';
import {StyleSheet, View} from 'react-native';
import {NavigationProps} from '../../../share/inrefaces';
import Header from '../../../share/components/Header';
import LogInSignInTitle from './LogInSignInTitle';
import {CustomInput} from './CustomInput';
import {UbuntuBold} from '../../../share/fonts';
import {FooterElement} from './FooterElement';
import store from '../../../stores';
import {observer} from 'mobx-react';

@observer
export default class Login extends React.Component<NavigationProps> {
  logIn() {
    store.loginStore
      .handleLogin()
      .then(() => this.props.navigation.navigate('Main'));
  }

  render() {
    const {
      loadData,
      emailErrorMessage,
      passwordErrorMessage,
      email,
      password,
      changeLogin,
      changePassword,
      onEmptyInputsFunction,
      clearInfo,
    } = store.loginStore;
    return (
      <View style={styles.container}>
        <Header />
        <LogInSignInTitle />
        <View
          style={{
            marginTop: 60,
          }}>
          <CustomInput
            disableInputs={loadData}
            errorMessage={emailErrorMessage}
            value={email}
            title={'Email'}
            onChangeText={email => changeLogin(email)}
          />
          <CustomInput
            disableInputs={loadData}
            errorMessage={passwordErrorMessage}
            value={password}
            style={{marginTop: 16}}
            secureTextEntry={true}
            title={'Пароль'}
            onChangeText={password => changePassword(password)}
          />
        </View>
        <FooterElement
          loadData={loadData}
          typeRegistration={false}
          onPressText={() => {
            this.props.navigation.navigate('SignUp');
            clearInfo();
          }}
          onPressButton={
            email !== '' && password !== '' ? this.logIn : onEmptyInputsFunction
          }
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8,
  },
  miniHeader: {
    fontSize: 13,
    color: 'rgba(37, 45, 118, 0.5)',
    paddingTop: 18,
    fontFamily: UbuntuBold,
  },
});
