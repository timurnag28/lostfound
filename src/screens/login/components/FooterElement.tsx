import React, {useState} from 'react';
import {
  ActivityIndicator,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {MontserratMedium, QuicksandBold} from '../../../share/fonts';
import {CustomCheckBox} from '../../../share/components/CustomCheckBox';

export const FooterElement = ({
  style,
  typeRegistration,
  onPressText,
  onPressButton,
  loadData,
}: {
  title?: string;
  style?: StyleProp<ViewStyle>;
  typeRegistration: boolean;
  onPressText: () => void;
  onPressButton: () => void;
  loadData: boolean;
}) => {
  const [checkBoxValue, setCheckBoxValue] = useState(false);

  return (
    <View style={[{marginTop: 48}, style]}>
      {!typeRegistration ? (
        <View style={{alignItems: 'center'}}>
          <Text style={styles.questionTitle}>Еще не зарегистрированы?</Text>
          <TouchableOpacity style={{paddingTop: 6}} onPress={onPressText}>
            <Text style={styles.registrationTitle}>Регистрация</Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled={loadData}
            style={[
              {
                backgroundColor: !loadData
                  ? '#3168DE'
                  : 'rgba(48, 102, 224, 0.43)',
              },
              styles.button,
            ]}
            onPress={onPressButton}>
            <Text style={styles.buttonText}>Войти</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={{alignItems: 'center'}}>
          <View style={{flexDirection: 'row'}}>
            <CustomCheckBox
              onPress={() => setCheckBoxValue(!checkBoxValue)}
              selected={checkBoxValue}
            />
            <Text style={styles.politics}>
              Я согласен с{' '}
              <Text style={{color: 'blue'}} onPress={onPressText}>
                Политикой {'\n'} конфеденциальности
              </Text>
            </Text>
          </View>
          <TouchableOpacity
            disabled={!checkBoxValue}
            style={[
              {
                backgroundColor: checkBoxValue
                  ? '#3168DE'
                  : 'rgba(48, 102, 224, 0.43)',
              },
              styles.button,
            ]}
            onPress={onPressButton}>
            <Text style={styles.buttonText}>Регистрация</Text>
          </TouchableOpacity>
        </View>
      )}
      {loadData ? (
        <ActivityIndicator
          style={{marginTop: 16}}
          size="large"
          color={'#3f5df5'}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  questionTitle: {
    fontFamily: MontserratMedium,
    color: '#929292',
    fontSize: 14,
  },
  registrationTitle: {
    fontFamily: MontserratMedium,
    color: '#3066E0',
    fontSize: 14,
  },
  button: {
    paddingHorizontal: 50,
    paddingVertical: 8,
    borderRadius: 22,
    marginTop: 50,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: QuicksandBold,
    fontSize: 14,
  },
  politics: {
    fontFamily: MontserratMedium,
    fontSize: 14,
    color: '#252D76',
    paddingLeft: 10,
  },
});
