import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import firebase from 'react-native-firebase';
import Header from '../share/components/Header';
import MapView, {Marker} from 'react-native-maps';
import {QuicksandBold} from '../share/fonts';
import store from '../stores';
import {observer} from 'mobx-react';
import {NavigationProps} from '../share/inrefaces';
import FastImage from 'react-native-fast-image';

@observer
export default class Main extends React.Component<NavigationProps> {
  componentDidMount() {
    store.mainStore.getUserPosition();
  }

  render() {
    const {locationUser} = store.mainStore;
    return (
      <View style={{flex: 1}}>
        <Header
          headerMiddleTitle={'Будем знакомы ;)'}
          headerBottomTitle={'Вы здесь'}
        />
        <MapView
          style={{flex: 1}}
          showsUserLocation={true}
          region={{
            latitude: locationUser ? locationUser[0] : 55.830433,
            longitude: locationUser ? locationUser[1] : 49.066082,
            latitudeDelta: 0.01,
            longitudeDelta: 0.01,
          }}>
          <Marker
            coordinate={{
              latitude: locationUser ? locationUser[0] : 55.830433,
              longitude: locationUser ? locationUser[1] : 49.066082,
            }}
            title={'точность'}
            description={'хромает'}>
            <FastImage
              resizeMode={'contain'}
              source={require('../../assets/images/mark.png')}
              style={{width: 50, height: 50}}
            />
          </Marker>
        </MapView>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              firebase
                .auth()
                .signOut()
                .then(() => this.props.navigation.navigate('Login'));
            }}>
            <Text style={styles.buttonTitle}>Понятно, я пойду</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    height: 140,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3066E0',
  },
  button: {backgroundColor: '#FFFFFF', borderRadius: 22},
  buttonTitle: {
    paddingHorizontal: 14,
    paddingVertical: 12,
    color: '#3066E0',
    fontSize: 14,
    fontFamily: QuicksandBold,
  },
});
