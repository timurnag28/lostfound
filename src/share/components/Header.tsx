import React, {PureComponent} from 'react';
import {
  View,
  StyleProp,
  ViewStyle,
  SafeAreaView,
  Text,
  StyleSheet,
} from 'react-native';
import {
  MontserratBold,
  MontserratRegular,
  QuicksandRegular,
  RobotoSlabBold,
} from '../fonts';

interface HeaderProps {
  style?: StyleProp<ViewStyle>;
  headerMiddleTitle?: string;
  headerBottomTitle?: string;
}

export default class Header extends PureComponent<HeaderProps> {
  render() {
    const {style, headerMiddleTitle, headerBottomTitle} = this.props;
    return (
      <SafeAreaView
        style={[
          styles.container,
          {
            height: !headerMiddleTitle ? 80 : 232,
          },
          style,
        ]}>
        <View style={style}>
          <View style={styles.textContainer}>
            <Text style={styles.logoContainer}>
              L<Text style={{color: '#E5E5E5'}}>F</Text>
            </Text>
            <View style={{flexDirection: 'column', paddingLeft: 14}}>
              <Text style={styles.title}>потерял</Text>
              <Text style={styles.secondTitle}>нашел</Text>
            </View>
          </View>
          {headerMiddleTitle ? (
            <View
              style={{
                alignSelf: 'center',
              }}>
              <Text style={styles.headerMidTitle}>{headerMiddleTitle}</Text>
            </View>
          ) : null}
          {headerBottomTitle ? (
            <View style={styles.headerBottomTitle}>
              <Text style={{color: '#FFFFFF', fontFamily: QuicksandRegular}}>
                {headerBottomTitle}
              </Text>
            </View>
          ) : null}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3066E0',
    zIndex: 100,
    width: '100%',
    justifyContent: 'flex-end',
  },
  textContainer: {
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 9,
  },
  logoContainer: {
    fontSize: 49,
    fontFamily: MontserratBold,
    color: '#F7D352',
  },
  title: {
    fontFamily: MontserratRegular,
    fontSize: 16,
    color: '#F7D352',
  },
  secondTitle: {
    fontFamily: MontserratRegular,
    fontSize: 16,
    color: '#E5E5E5',
  },
  headerMidTitle: {
    fontSize: 29,
    fontFamily: RobotoSlabBold,
    color: '#FFFFFF',
  },
  headerBottomTitle: {
    alignSelf: 'center',
    paddingTop: 23,
    paddingBottom: 9,
  },
});
