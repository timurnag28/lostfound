import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {TouchableOpacity, StyleProp, ViewStyle} from 'react-native';

export const CustomCheckBox = ({
  selected,
  onPress,
  style,
  size = 21,
}: {
  selected: boolean;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  size?: number;
}) => (
  <TouchableOpacity style={style} onPress={onPress}>
    <Icon
      size={size}
      color={selected ? '#3066E0' : 'gray'}
      name={selected ? 'checkbox-marked' : 'checkbox-blank-outline'}
    />
  </TouchableOpacity>
);
