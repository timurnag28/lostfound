import {createStackNavigator} from 'react-navigation-stack';
import Login from '../screens/login/components/Login';
import SignUp from '../screens/signIn/components/SignUp';

export default createStackNavigator(
  {
    Login: {
      screen: Login,
    },
    SignUp: {
      screen: SignUp,
    },
  },
  {
    initialRouteName: 'SignUp',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#ffffff',
      },
    },
  },
);
