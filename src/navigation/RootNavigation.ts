import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import LoginStack from './LoginStack';
import MainStack from './MainStack';
import LoadingStack from './LoadingStack';

const AppNavigator = createSwitchNavigator(
  {
    loadingStack: LoadingStack,
    loginStack: LoginStack,
    mainStack: MainStack,
  },

  {
    initialRouteName: 'loadingStack',
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
