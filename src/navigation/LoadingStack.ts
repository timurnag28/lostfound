import {createStackNavigator} from 'react-navigation-stack';
import Loading from '../screens/Loading';

export default createStackNavigator(
  {
    Loading: {
      screen: Loading,
    },
  },
  {
    initialRouteName: 'Loading',
    defaultNavigationOptions: {
      header: () => null,
      cardStyle: {
        backgroundColor: '#ffffff',
      },
    },
  },
);
