import {action, observable} from 'mobx';
import API from '../api';

class LoginStore {
  @observable email: string = '';
  @observable password: string = '';
  @observable emailErrorMessage: string = '';
  @observable passwordErrorMessage: string = '';
  @observable loadData: boolean = false;
  @observable onRefEmail: any = null;
  @observable onRefPassword: any = null;

  @action
  changeLogin = (item: string) => {
    this.email = item;
  };
  @action
  changePassword = (item: string) => {
    this.password = item;
  };

  @action
  onEmptyInputsFunction = () => {
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';
    this.email === '' ? (this.emailErrorMessage = 'Введите email') : null;
    this.password === ''
      ? (this.passwordErrorMessage = 'Введите пароль')
      : null;
    this.loadData = false;
  };

  @action
  handleLogin = () => {
    this.loadData = true;
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';

    return API.auth
      .login(this.email, this.password)
      .then((response: any) => {
        this.loadData = false;
        this.clearInfo();
        return response;
      })
      .catch((error: {message: string}) => {
        if (error.message === 'Неверный пароль!') {
          this.passwordErrorMessage = error.message;
        } else {
          this.emailErrorMessage = error.message;
        }
        this.loadData = false;
      });
  };

  @action
  clearInfo = () => {
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';
    this.email = '';
    this.password = '';
  };
}

const loginStore = new LoginStore();
export default loginStore;
