import {action, observable} from 'mobx';
import API from '../api';
import ImagePicker from 'react-native-image-crop-picker';
class SignInStore {
  @observable email: string = '';
  @observable password: string = '';
  @observable emailErrorMessage: string = '';
  @observable passwordErrorMessage: string = '';
  @observable loadData: boolean = false;
  @observable avatar: string = '';

  @action
  changeRegLogin = (item: string) => {
    this.email = item;
  };
  @action
  changeRegPassword = (item: string) => {
    this.password = item;
  };

  @action
  onEmptyInputsFunction = () => {
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';
    this.email === '' ? (this.emailErrorMessage = 'Введите email') : null;
    this.password === ''
      ? (this.passwordErrorMessage = 'Введите пароль')
      : null;
    this.loadData = false;
  };

  @action
  handleSignUp = () => {
    this.loadData = true;
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';

    return API.auth
      .sign(this.email, this.password)
      .then((response: any) => {
        this.loadData = false;
        this.clearInfo();
        return response;
      })
      .catch((error: {message: string}) => {
        console.log('catch detected ' + error);
        if (error.message === 'Слабый пароль!') {
          this.passwordErrorMessage = error.message;
        } else {
          this.emailErrorMessage = error.message;
        }
        this.loadData = false;
      });
  };

  @action
  setAvatar = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
    }).then(image => {
      console.log(image);
      if ("path" in image) {
        this.avatar = image.path;
      }
    });
  };

  @action
  deleteAvatar = () => {
    this.avatar = '';
  };

  @action
  clearInfo = () => {
    this.emailErrorMessage = '';
    this.passwordErrorMessage = '';
    this.email = '';
    this.password = '';
  };
}

const signInStore = new SignInStore();
export default signInStore;
