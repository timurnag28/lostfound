import {action, observable} from 'mobx';

import Geolocation from '@react-native-community/geolocation';
class MainStore {
  @observable permission: any;
  @observable locationUser: any;
  @observable error: string = '';

  @action
  getUserPosition = () => {
    Geolocation.getCurrentPosition(
      info => {
        this.locationUser = [info.coords.latitude, info.coords.longitude];
      },
      error => console.log(error),
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
    );
  };
}

const mainStore = new MainStore();
export default mainStore;
